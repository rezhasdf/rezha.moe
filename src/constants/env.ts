export const env = {
  IS_IN_PRODUCTION: process.env.NODE_ENV === 'production',
  GITHUB_ID: process.env.GITHUB_ID as string,
  GITHUB_SECRET: process.env.GITHUB_SECRET as string,
  LINKEDIN_ID: process.env.LINKEDIN_ID as string,
  LINKEDIN_SECRET: process.env.LINKEDIN_SECRET as string,
} as const
