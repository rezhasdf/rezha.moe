import { NextSeo } from 'next-seo'

export default function Home(): JSX.Element {
  return (
    <>
      <NextSeo title="Home" />
      <div className="flex flex-col items-center justify-center min-h-screen px-2 py-0">
        <main className="flex flex-col items-center justify-center flex-1 px-20 py-0">
          ...
        </main>
      </div>
    </>
  )
}

Home.layoutProps = {
  Layout: (props: unknown) => (
    <div style={{backgroundColor: 'blue'}}>
      <div className=""  { ...props }/>
    </div>
  ),
}
